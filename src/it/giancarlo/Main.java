package it.giancarlo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import org.apache.http.client.ClientProtocolException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import eu.specs.datamodel.broker.BrokerTemplateBean;
import eu.specs.datamodel.broker.ClusterNode;
import eu.specs.datamodel.broker.NodeCredential;
import eu.specs.datamodel.broker.NodeCredentialsManager;
import eu.specs.datamodel.broker.NodesInfo;
import eu.specs.datamodel.broker.ProviderCredential;
import eu.specs.datamodel.broker.ProviderCredentialsManager;
import eu.specs.datamodel.enforcement.ImplementationPlan;
import eu.specs.project.enforcement.broker.ChefServiceImpl;
import eu.specs.project.enforcement.broker.ComputeServiceAmazon;
import eu.specs.project.enforcement.broker.ComputeServiceEucalyptus;
import eu.specs.project.enforcement.broker.interfaces.ComputeServiceInterface;
import eu.specs.project.enforcement.contextlistener.ContextListener;

public class Main {
	private static String databagId = "";

	public static void main(String[] args) {

		UUID idOne = UUID.randomUUID();
		System.out.println("UUID generato vale: "+idOne);
		databagId = idOne.toString();
		//		databagId = "plan-id-1";
		//		acquireAmazonVms();
		//		executeScriptOnNode();
		//		getTokenFromVM(user, host, port);	
		//		ArrayList<String> privateIp = new ArrayList<String>();
		//		privateIp.add("192.168.1.1");
		//		myplan.getPools().get(0).getVms().get(0).getComponents().get(0).setPrivateIps(privateIp);
		//		System.out.println("ip che carico nel plan: "+privateIp);
		//		System.out.println("piano: "+new Gson().toJson(myplan));
		//		chefService.updateDatabagItem("implementation_plans", myplan.getId(), new Gson().toJson(myplan).toString());

		ImplementationPlan myplan;
		try {

			myplan = parsePlan();
			myplan.setId(databagId);

			System.out.println("myplan: "+myplan.toString());

			ObjectMapper mapper = new ObjectMapper();
			try {
				String jsonInString = mapper.writeValueAsString(myplan);
				System.out.println("jsonInString: "+jsonInString.toString());

				ChefServiceImpl chef = new ChefServiceImpl();

				chef.uploadDatabagItem("implementation_plans", myplan.getId(),  jsonInString);
				System.out.println(chef.getDatabagItem("implementation_plans", myplan.getId()).toString());
			} catch (JsonProcessingException e2) {
				e2.printStackTrace();
			}


			if(myplan!=null)
				acquireEucalyptusVMs(myplan);
			else
				System.out.println("il piano e' nullo");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private static String[] prepareEnvironmentEucalyptusScript() {
		String[] script= {
				"/opt/mos-chef-client/bin/check-chef-server.sh"
		};
		return script;
	}

	private static String[] prepareEnvironmentAmazonScript() {
		String[] script= {
				"zypper -n --gpg-auto-import-keys in curl" ,
				"curl -O http://curl.haxx.se/ca/cacert.pem",
				"mv cacert.pem /etc/ca-certificates/",
				"touch .bashrc",
				"echo 'export CURL_CA_BUNDLE=/etc/ca-certificates/cacert.pem' > .bashrc"
		};
		return script;
	}


	private static ImplementationPlan parsePlan() throws IOException {

		ObjectMapper mapper = new ObjectMapper();
		ImplementationPlan myplan;
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader (Main.class.getClassLoader().getResourceAsStream("plan.txt"), "UTF8")); 
			String value="";
			String tmp;
			while (( tmp = br.readLine()) != null) {
				value+=(tmp+"\n");
			}
			br.close();


			myplan = mapper.readValue(value, ImplementationPlan.class);
			System.out.println("myplan.getId(): "+myplan.getId());
			return myplan;
		} catch (JsonParseException e) {
			e.printStackTrace();
			throw e;
		} catch (JsonMappingException e) {
			e.printStackTrace();
			throw e;
		}
	}


	private static void executeScriptOnNode(){

		BrokerTemplateBean bean = new BrokerTemplateBean();
		bean.setAppliance("us-east-1/ami-ff0e0696");
		bean.setHw("t1.micro");
		bean.setProvider("aws-ec2");
		bean.setZone("us-east-1");

		ProviderCredential provCred = new ProviderCredential("AKIAJUXYVK7J5IUFDKUA","XbCQCfWYXzcD82K/Cx20XY8TSHUwxSo/atvpIyLF");
		ProviderCredentialsManager.add("provider", provCred);
		ComputeServiceAmazon cloudservice = new ComputeServiceAmazon(bean.getProvider(), "root", provCred);		

		ClusterNode c = new ClusterNode("us-east-1/i-42a06cf4", "54.86.37.132", "172.31.0.183");
		ContextListener cc = new ContextListener();
		NodeCredential myCred = cc.brokerCredentials();
		NodeCredential cred = new NodeCredential("specs.123456", 
				myCred.getPublickey(), 
				myCred.getPrivatekey());

		Thread[] threads = new Thread[1];
		for (int i = 0; i < threads.length; i++) {
			try {
				threads[i] =  cloudservice.threadExecuteInstructionsOnNode("root",c,prepareEnvironmentAmazonScript(), cred.getPrivatekey(),false);
				threads[i].start();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
	}

	private static void acquireAmazonVms(){
		// inserisco i valori che descrivono il provider e le sue credenziali
		BrokerTemplateBean bean = new BrokerTemplateBean();
		bean.setAppliance("us-east-1/ami-ff0e0696");
		bean.setHw("t1.micro");
		bean.setProvider("aws-ec2");
		bean.setZone("us-east-1");

		ProviderCredential provCred = new ProviderCredential("AKIAJUXYVK7J5IUFDKUA","XbCQCfWYXzcD82K/Cx20XY8TSHUwxSo/atvpIyLF");
		ProviderCredentialsManager.add("provider", provCred);
		CloudService cloudservice = new CloudService(bean.getProvider(), "root", provCred);		

		java.util.Date date= new java.util.Date();	
		System.out.println(new Timestamp(date.getTime())+" | VMs creating");	


		//		InstanceDescriptor descr = new InstanceDescriptor(bean.appliance, bean.zone, bean.hw);

		String group = "sla-1";

		ContextListener cc = new ContextListener();
		NodeCredential myCred = cc.brokerCredentials();
		System.out.println("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		System.out.println("public key: "+myCred.getPublickey());

		NodeCredential cred = new NodeCredential("specs.123456", 
				myCred.getPublickey(), 
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		try {
			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, 1 , bean,
					NodeCredentialsManager.getCredentials("def"),22,80,11211, 9390,1514);

			java.util.Date myDate= new java.util.Date();	
			System.out.println(new Timestamp(myDate.getTime())+" | VMs created");

			//-------Update Implementation-Info with nodes info----------------
			//			info.status=Status.VMs_Acquired;
			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				System.out.println("ip privato acquisito: "+s);
				//				info.private_IP_Address.add(s);
				//				info.public_IP_Address.add(node.getPublicIP());

			}

			//-----------Preparing VMs : install curl--------------------
			java.util.Date mydatenew= new java.util.Date();	
			System.out.println(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				threads[i] = cloudservice.new ExecuteIstructionsOnNode("root",nodes.get(i),prepareEnvironmentAmazonScript(), cred.getPrivatekey(),false,cloudservice);
				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			java.util.Date date1= new java.util.Date();	
			System.out.println(new Timestamp(date1.getTime())+" | VMs prepared");


			//			info.status=Status.VMs_Prepared;


			//attributo del nodo che viene letto dalla ricetta
			String attribute= "{\"implementation_plan_id\":\""+databagId+"\"}";		


			java.util.Date date4= new java.util.Date();	   
			System.out.println(new Timestamp(date4.getTime())+" | chef node bootstrapping");

			ChefService chefService = new ChefService(null, null, null, null, null);
			chefService.bootstrapChef(group, nodesInfo, cloudservice,attribute);

			java.util.Date date5= new java.util.Date();	
			System.out.println(new Timestamp(date5.getTime())+" | chef node bootstrapped");

			//			info.status=Status.Chef_Node_Bootstrapped;

			//			for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());


			List<List<String>> recipes=new ArrayList<List<String>> ();
			List <String> temp = new ArrayList<String>();
			temp.add("specs-enforcement-webpool::nginx");
			recipes.add(temp);

			//---------------------Executing recipes on nodes ------------------------------------------
			for(int i=0;i<nodes.size();i++ ){


				java.util.Date date6= new java.util.Date();	
				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(i).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				

				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(i), group,myCred.getPrivatekey(), cloudservice);
				threads[i].start();

				//				info.recipes.add(InfoRecipes(recipes.get(i)));

			}

			for(int i=0;i<nodes.size();i++ ){
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			if(true){
				java.util.Date date7= new java.util.Date();		
				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			}

			//			info.status=Status.Recipes_completed;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	//	private static void uploadDatabag(){
	//		String databagId = UUID.randomUUID().toString().replace("-","");
	//		try {
	//			chefService.uploadDatabagItem("implementation_test",  databagId, "{\"plan_id\":\"1\",\"value\":\"30\"}");
	//		} catch (ResourceNotFoundException e) {
	//			e.printStackTrace();
	//		}
	//	}

	private static void acquireEucalyptusVMs(ImplementationPlan plan){
		BrokerTemplateBean bean = new BrokerTemplateBean();
		bean.setAppliance(plan.getIaas().getAppliance());
		bean.setHw(plan.getIaas().getHardware());
		bean.setProvider(plan.getIaas().getProvider());
		bean.setZone(plan.getIaas().getZone());

		//AMAZON
		//		ProviderCredential provCred = new ProviderCredential("AKIAJUXYVK7J5IUFDKUA","XbCQCfWYXzcD82K/Cx20XY8TSHUwxSo/atvpIyLF");
		//EUCALYPTUS
		//		ProviderCredential provCred = new ProviderCredential("AKIE5G1IFU0ZOWY6VQOQ","Hx8aDZ41f4WV55xmpGiHXLyzsWvDBy9UCvk2n4Ri");

		ContextListener cc = new ContextListener();
		//		BrokerTemplateBean bean = cc.getDefaultBrokerTemplateBean();
		//		BrokerTemplateBean bean = new BrokerTemplateBean();
		//		bean.setAppliance(plan.getIaas().getZone()+"/"+plan.getIaas().getAppliance());
		//		bean.setHw(plan.getIaas().getHardware());
		//		bean.setProvider(plan.getIaas().getProvider());
		//		bean.setZone(plan.getIaas().getZone());


		ProviderCredential provCred = cc.getDefaultProviderCredentials();
		ProviderCredentialsManager.add("provider", provCred);
		ComputeServiceInterface cloudservice = new ComputeServiceEucalyptus(bean.getProvider(), "root", provCred);		

		java.util.Date date= new java.util.Date();
		System.out.println(new Timestamp(date.getTime())+" | VMs creating");

		String group = "sla-"+plan.getId();

		NodeCredential myCred = cc.brokerCredentials();
		//		System.out.println("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		//		System.out.println("public key: "+myCred.getPublickey());
		//		System.out.println("private key: "+myCred.getPrivatekey());

		NodeCredential cred = new NodeCredential(
				myCred.getPublickey(), 
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		int vmsNumber = plan.getPools().get(0).getVms().size();


		try {
			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, vmsNumber , bean,
					NodeCredentialsManager.getCredentials("def"), 22, 80, 8080, 11211, 9390, 1514);

			//			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, 1 , descr,
			//					NodeCredentialsManager.getCredentials("def"),22,80,11211, 9390,1514);

			java.util.Date myDate= new java.util.Date();	
			System.out.println(new Timestamp(myDate.getTime())+" | VMs created");

			//-------Update Implementation-Info with nodes info----------------
			//			info.status=Status.VMs_Acquired;
			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				System.out.println("ip privato acquisito: "+s);
				System.out.println("ip pubblico acquisito: "+node.getPublicIP());
				System.out.println("node id: "+node.getId());
				//				info.private_IP_Address.add(s);
				//				info.public_IP_Address.add(node.getPublicIP());

			}

			//-----------Preparing VMs : executing environment script--------------------
			java.util.Date mydatenew= new java.util.Date();	
			System.out.println(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				//				threads[i] = cloudservice.threadExecuteInstructionsOnNode("root",nodes.get(i),prepareEnvironmentEucalyptusScript(), cred.getPrivatekey(), true);
				threads[i] = cloudservice.threadExecuteInstructionsOnNode("root",nodes.get(i),new String[]{"zypper -n install ruby"}, cred.getPrivatekey(), true);

				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {
					ignore.printStackTrace();
				}
			}



			java.util.Date date1= new java.util.Date();	
			System.out.println(new Timestamp(date1.getTime())+" | VMs prepared");

			String attribute= "{\"implementation_plan_id\":\""+databagId+"\"}";	
			//			ChefService chefService = new ChefService(null, null, null, null, null);
			ChefServiceImpl chefService = new ChefServiceImpl();
			chefService.bootstrapChef(group, nodesInfo, cloudservice, attribute);
			//
			java.util.Date date5= new java.util.Date();	
			System.out.println(new Timestamp(date5.getTime())+" | chef node bootstrapped");


			//---------------------Executing recipes on nodes ------------------------------------------
			for(int i=0;i<nodes.size();i++){

				List<List<String>> recipes=new ArrayList<List<String>> ();
				List <String> temp = new ArrayList<String>();
				temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(0).getCookbook()
						+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(0).getRecipe());
				//				temp.add("applications::metric-catalogue-app");
				//				temp.add("specs-enforcement-webpool::nginx");
				recipes.add(temp);

				java.util.Date date6= new java.util.Date();	
				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(0).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP());				
				final int current = i;
				
				ChefServiceImpl chef = new ChefServiceImpl();
				ArrayList<String> privateIp = new ArrayList<String>();
				privateIp.add(nodes.get(current).getPrivateIP());
				plan.getPools().get(0).getVms().get(current).getComponents().get(0).setPrivateIps(privateIp);
				System.out.println("ip che carico nel plan: "+privateIp);
				//		chef.uploadDatabagItem("SPECS", "implementation_plan", new Gson().toJson(plan.toString()));

				ObjectMapper mapper = new ObjectMapper();
				try {
					String jsonInString = mapper.writeValueAsString(plan);
					chef.updateDatabagItem("implementation_plans", plan.getId(), jsonInString);

					//			System.out.println(chef.getDatabagItem("SPECS", "implementation_plans").toString());
				} catch (JsonProcessingException e2) {
					e2.printStackTrace();
				}

				threads[i] = chefService.threadExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group, myCred.getPrivatekey(), cloudservice);
				threads[i].start();


				//				
				//				threads[i]= new Thread() {
				//
				//					@Override
				//					public void run() {
				//						super.run();
				//						String rec = "";
				//						for(int i=0;i<recipes.get(0).size();i++){
				//							rec=recipes.get(0).get(i);
				//						}
				//						final String recipes = rec;
				//						String path = ContextListener.CHEF_SERVER_ENDPOINT.replace("/organizations/", "").replace("https:", "http:")+
				//								":81/mos/cgi/mos-chef-server-core/chef-server-apply-recipe.cgi?token="+
				//								ComputeServiceEucalyptus.EUCALYPTUS_CHEF_SERVER_TOKEN+
				//								"&node="+nodes.get(current).getId().replace("eucalyptus/", "")+"&recipe="+recipes;
				//
				//						ChefServiceImpl chef = new ChefServiceImpl();
				////						chef.getDatabagItem("SPECS", "implementation_plan");
				////						chef.uploadDatabagItem("SPECS", "implementation_plan", "{\"test\":\"test\"}");
				//						ArrayList<String> privateIp = new ArrayList<String>();
				//						privateIp.add(nodes.get(current).getPublicIP());
				//						plan.getPools().get(0).getVms().get(current).getComponents().get(0).setPrivateIps(privateIp);
				//						System.out.println("ip che carico nel plan: "+privateIp);
				////						chef.uploadDatabagItem("SPECS", "implementation_plan", new Gson().toJson(plan.toString()));
				//						
				//						ObjectMapper mapper = new ObjectMapper();
				//						try {
				//							String jsonInString = mapper.writeValueAsString(plan);
				//							chef.updateDatabagItem("implementation_plans", plan.getId(), jsonInString);
				//
				////							System.out.println(chef.getDatabagItem("SPECS", "implementation_plans").toString());
				//						} catch (JsonProcessingException e2) {
				//							e2.printStackTrace();
				//						}
				//
				//						
				//						
				//						System.out.println("*** path ricette chef **** e': "+path);
				//						String res;
				//						try {
				//							res = ContextListener.doGet(path);
				//							//						String res = ContextListener.doGetUncheckSSL(path);
				//							JsonParser parser = new JsonParser();
				//							JsonObject o = (JsonObject)parser.parse(res);
				//
				//							//							if(o.get("message").equals("ok"))
				//							//								System.out.println("recipe executed properly");
				//							//							else{
				//							//								System.out.println("ERROR in recipe execution: "+res);
				//							long d = 30000;
				//							for(int i=0;i<10;i++){
				//								try {
				//									if(i!=0){
				//										System.out.println("prima di sleep: "+i);
				//										sleep(d);
				//										System.out.println("dopo sleep: "+i);
				//									}
				//
				//									String res2 = ContextListener.doGet(path);
				//									System.out.println("res della get: "+res2);
				//									JsonObject obj = (JsonObject)parser.parse(res2);
				//									System.out.println("obj della get: "+obj.toString());
				//									System.out.println("obj.get message: "+obj.get("message"));
				//									if(obj.get("message").toString().equals("ok")
				//											|| obj.get("message").toString().equals("\"ok\"")){
				//										System.out.println("recipe executed properly");
				//										break;
				//									}
				//								} catch (Exception e1) {
				//									System.out.println("si è verificato un catch nel catch di ExecuteIstructionsOnNode: "+i);
				//									e1.printStackTrace();
				//								}
				//							}
				//							//							}
				//						} catch (ClientProtocolException e) {
				//							e.printStackTrace();
				//						} catch (IOException e) {
				//							e.printStackTrace();
				//						}
				//					}
				//
				//
				//				};

				//				threads[i]= new Thread(new Runnable() {
				//
				//					@Override
				//					public void run() {
				//						String rec = "";
				//						for(int i=0;i<recipes.get(current).size();i++){
				//							rec=recipes.get(current).get(i);
				//						}
				//						final String recipes = rec;
				//						String path = ContextListener.CHEF_SERVER_ENDPOINT.replace("/organizations/", "").replace("https:", "http:")+
				//								":81/mos/cgi/mos-chef-server-core/chef-server-apply-recipe.cgi?token="+
				//								CloudServiceEucalyptus.EUCALYPTUS_CHEF_SERVER_TOKEN+
				//								"&node="+nodes.get(current).getId().replace("eucalyptus/", "")+"&recipe="+recipes;
				//
				//						System.out.println("path ricette chef e': "+path);
				//						String res;
				//						try {
				//							res = ContextListener.doGet(path);
				//							//						String res = ContextListener.doGetUncheckSSL(path);
				//							JsonParser parser = new JsonParser();
				//							JsonObject o = (JsonObject)parser.parse(res);
				//
				//							if(o.get("message").equals("ok"))
				//								System.out.println("recipe executed properly");
				//							else{
				//								System.out.println("ERROR in recipe execution: "+res);
				//								long d = 10000;
				//								for(int i=0;i<10;i++){
				//									try {
				//										System.out.println("prima di sleep: "+i);
				//										sleep(d);
				//										System.out.println("dopo sleep: "+i);
				//
				//										compute.executeScriptOnNode(user, node,istructions,privateKey,sudo);
				//										break;
				//									} catch (Exception e1) {
				//										System.out.println("si è verificato un catch nel catch di ExecuteIstructionsOnNode: "+i);
				//										e1.printStackTrace();
				//									}
				//								}
				//							}
				//						} catch (ClientProtocolException e) {
				//							e.printStackTrace();
				//						} catch (IOException e) {
				//							e.printStackTrace();
				//						}
				//					}
				//				});
				//				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group,myCred.getPrivatekey(), cloudservice);
				//				threads[i].start();

				//				info.recipes.add(InfoRecipes(recipes.get(i)));
			}

			for(int i=0;i<nodes.size();i++ ){
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			if(true){
				java.util.Date date7= new java.util.Date();		
				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			}

			//			info.status=Status.Recipes_completed;



			//
			//
			//			//			info.status=Status.VMs_Prepared;
			//
			//
			//			//attributo del nodo che viene letto dalla ricetta
			//			String attribute= "{\"implementation_plan_id\":\""+databagId+"\"}";		
			//
			/////* the following part is commented since it works only on Amazon
			//			java.util.Date date4= new java.util.Date();	   
			//			System.out.println(new Timestamp(date4.getTime())+" | chef node bootstrapping");
			//
			//
			//			chefService.bootstrapChef(group, nodesInfo, cloudservice,attribute);
			//
			//			java.util.Date date5= new java.util.Date();	
			//			System.out.println(new Timestamp(date5.getTime())+" | chef node bootstrapped");
			//
			//			//			info.status=Status.Chef_Node_Bootstrapped;
			//
			////			for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());
			////*/
			//			//---------------------Executing recipes on nodes ------------------------------------------
			//			for(int i=0;i<nodes.size();i++ ){
			//
			//				List<List<String>> recipes=new ArrayList<List<String>> ();
			//				List <String> temp = new ArrayList<String>();
			////				temp.add(plan.getPools().get(0).getVms().get(i).getComponents().get(0).getCookbook()
			////						+"::"+plan.getPools().get(0).getVms().get(i).getComponents().get(0).getRecipe());
			//				temp.add("applications::web-container-app");
			////				temp.add("specs-enforcement-webpool::nginx");
			//				recipes.add(temp);
			//
			//				java.util.Date date6= new java.util.Date();	
			//				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(0).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				
			//
			//				threads[i] = chefService.new ExecuteRecipesOnNode(nodes.get(i), recipes.get(0), group,myCred.getPrivatekey(), cloudservice);
			//				threads[i].start();
			//
			////				info.recipes.add(InfoRecipes(recipes.get(i)));
			//			}
			//
			//			for(int i=0;i<nodes.size();i++ ){
			//				try {
			//					threads[i].join();
			//				} catch (InterruptedException ignore) {}
			//			}
			//
			//			if(true){
			//				java.util.Date date7= new java.util.Date();		
			//				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			//			}

			//			info.status=Status.Recipes_completed;

			// */
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private static void getTokenFromVM(String user, String host, int port){
		try {
			JSch jsch = new JSch();
			ContextListener cc = new ContextListener();

			//            ProviderCredential provCred = cc.getDefaultProviderCredentials();
			NodeCredential myCred = cc.brokerCredentials();
			String privateKey = myCred.getPrivatekey();

			jsch.addIdentity(user, privateKey.getBytes(),null, new byte[0]);
			System.out.println("identity added ");

			Session session = jsch.getSession(user, host, port);
			System.out.println("session created.");

			java.util.Properties config = new java.util.Properties();
			config.put("StrictHostKeyChecking", "no");
			session.setConfig(config);

			session.connect();
			System.out.println("session connected.....");

			ChannelExec channel = (ChannelExec) session.openChannel("exec");
			InputStream in = channel.getInputStream();
			OutputStream out = channel.getOutputStream();
			channel.setErrStream(System.err);

			String bootstrapCommand = "cat /mos/etc/mos/secret.json; "+
					"cat /mos/etc/mos/environment.json;";

			channel.setCommand(bootstrapCommand);
			channel.connect();
			//			channel.setOutputStream(System.out);

			out.write(("\n").getBytes());
			out.flush();


			byte[] tmp = new byte[1024];
			while(true){

				while(in.available()>0){
					int i = in.read(tmp, 0, 1024);
					if(i<0)
						break;
					System.out.println(new String(tmp, 0, i));
				}

				if(channel.isClosed()){
					int exitStatus = 123456789;
					exitStatus = channel.getExitStatus();
					if(exitStatus==0){
						System.out.println("EVERYTHING WENT FINE");
					}
					else{
						System.out.println("**********************************");
						System.out.println("*** ERROR **ERROR **ERROR **ERROR **ERROR **ERROR ***");
						System.out.println("*** CHECK LOGS TO CATCH THE ERROR ***");
						System.out.println("**********************************");
					}

					break;
				}
				try{
					Thread.sleep(1000);
				}catch(Exception e){

				}
			}

			System.out.println("fuori dal while");
			channel.disconnect();
			session.disconnect();

		} catch (Exception e) {
			System.err.println(e);
		}
	}

	private void connectViaSsh(){

	}

	/**
	 * acquires VMs from eucalyptus but uses ChefServer Hosted by Chef Manage IO
	 */
	/*
	private static void acquireEucalyptusVmsOLDPlan(){

		ContextListener cc = new ContextListener();
		BrokerTemplateBean bean = cc.getDefaultBrokerTemplateBean();

		ProviderCredential provCred = cc.getDefaultProviderCredentials();
		ProviderCredentialsManager.add("provider", provCred);
		CloudService cloudservice = new CloudService(bean.getProvider(), "root", provCred);		

		java.util.Date date= new java.util.Date();	
		System.out.println(new Timestamp(date.getTime())+" | VMs creating");	

		String group = "sla-cerict-";//+plan.getId();

		NodeCredential myCred = cc.brokerCredentials();
		//		System.out.println("le credenziali sono: "+(myCred==null ? "nulle":"non nulle"));
		//		System.out.println("public key: "+myCred.getPublickey());
		//		System.out.println("private key: "+myCred.getPrivatekey());

		NodeCredential cred = new NodeCredential(
				myCred.getPublickey(), 
				myCred.getPrivatekey());
		NodeCredentialsManager.add("def", cred);

		try {
			NodesInfo nodesInfo = cloudservice.createNodesInGroup(group, 1 , bean,
					NodeCredentialsManager.getCredentials("def"),22,80,11211, 9390,1514);

			java.util.Date myDate= new java.util.Date();	
			System.out.println(new Timestamp(myDate.getTime())+" | VMs created");

			//-------Update Implementation-Info with nodes info----------------
			//			info.status=Status.VMs_Acquired;
			List<ClusterNode> nodes=nodesInfo.getNodes();

			for(ClusterNode node : nodes){

				String s=node.getPrivateIP();
				System.out.println("ip privato acquisito: "+s);
				String s1=node.getPublicIP();
				System.out.println("ip pubblico acquisito: "+s1);
				System.out.println("id del nodo: "+node.getId());	
				//				info.private_IP_Address.add(s);
				//				info.public_IP_Address.add(node.getPublicIP());

			}

			//-----------Preparing VMs : install curl--------------------
			java.util.Date mydatenew= new java.util.Date();	
			System.out.println(new Timestamp(mydatenew.getTime())+" | VMs preparing");

			Thread[] threads = new Thread[nodes.size()];
			for (int i = 0; i < threads.length; i++) {
				threads[i] = cloudservice.new ExecuteIstructionsOnNode("root",nodes.get(i), prepareEnvironmentScriptAmazon(""), cred.getPrivatekey(), true, cloudservice);
				threads[i].start();
			}

			for (int i = 0; i < threads.length; i++) {
				try {
					threads[i].join();
				} catch (InterruptedException ignore) {}
			}

			java.util.Date date1= new java.util.Date();	
			System.out.println(new Timestamp(date1.getTime())+" | VMs prepared");

			//			info.status=Status.VMs_Prepared;

			//attributo del nodo che viene letto dalla ricetta
			String attribute= "{\"implementation_plan_id\":\""+databagId+"\"}";		


			java.util.Date date4= new java.util.Date();	   
			System.out.println(new Timestamp(date4.getTime())+" | chef node bootstrapping");

			ChefService chefService = new ChefService(null, null, null, null, null);
			chefService.bootstrapChef(group, nodesInfo, cloudservice, attribute);

			java.util.Date date5= new java.util.Date();	
			System.out.println(new Timestamp(date5.getTime())+" | chef node bootstrapped");

			//			info.status=Status.Chef_Node_Bootstrapped;

			//			for(ClusterNode node : nodes)  info.chefNodesName.add(group+"-"+node.getPrivateIP());


			List<List<String>> recipes=new ArrayList<List<String>> ();
			List <String> temp = new ArrayList<String>();
			temp.add("specs-enforcement-webpool::nginx");
			recipes.add(temp);

			//---------------------Executing recipes on nodes ------------------------------------------
			for(int i=0;i<nodes.size();i++ ){


				java.util.Date date6= new java.util.Date();	
				System.out.println(new Timestamp(date6.getTime())+" | executing recipes: "+Arrays.toString(recipes.get(i).toArray())+" on node "+group+"-"+nodes.get(i).getPrivateIP()+" publicIP:"+nodes.get(i).getPublicIP());				

//				threads[i] = new ExecuteRecipesOnNode(chefService.getChefApi(), nodes.get(i), recipes.get(i), group,myCred.getPrivatekey(), cloudservice);
//				threads[i].start();
				chefService.executeRecipesOnNode(nodes.get(i), recipes.get(i), group, cloudservice, myCred);
				//				info.recipes.add(InfoRecipes(recipes.get(i)));

			}

//			for(int i=0;i<nodes.size();i++ ){
//				try {
//					threads[i].join();
//				} catch (InterruptedException ignore) {}
//			}

			if(true){
				java.util.Date date7= new java.util.Date();		
				System.out.println(new Timestamp(date7.getTime())+" | recipes completed");
			}

			//			info.status=Status.Recipes_completed;


		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	 */

}
